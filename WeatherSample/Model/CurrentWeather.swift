//
//  CurrentWeather.swift
///  WeatherSample
//
//  Created by GEIDC on 1/5/19.
//  Copyright © 2019 Techmahindra. All rights reserved.
//

import UIKit
import Alamofire

/*
 // Writing CustomClass With CurrentWeather Details
 */
class CurrentWeather {
    
    var _cityName : String!
    var _date : String!
    var _weatherType : String!
    var _currentTemp: Double!
    var _humadity: Double!
    
    
    var cityName : String {
        if _cityName == nil {
            _cityName = ""
        }
        return _cityName
    }
    
    var date: String {
        if _date == nil {
            _date = ""
        }
        
        let dateFormatter = DateFormatter() // We are telling the formatter how it should look like
        
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .none
        let currentDate = dateFormatter.string(from: Date())
        self._date = "Today, \(currentDate)" // Creating a string which will be displayed on our screen.
        
        return _date
    }
    
    var weatherType: String {
        if _weatherType == nil {
            _weatherType = ""
        }
        return _weatherType
    }
    
    var currentTemp: Double {
        if _currentTemp == nil {
            _currentTemp = 0.0
        }
        return _currentTemp
    }
    var humadity: Double {
        if _humadity == nil {
            _humadity = 0.0
        }
        return _humadity
    }
    /*
     // Downloading Current wetaher forecast based on selected City
     */
    func downloadWeatherDetails(completed: @escaping DownloadComplete){
        // Alamofire download
        
        let searchString = UserDefaults.standard.string(forKey: "Key")!
        
        let trimmedString = searchString.trimmingCharacters(in: .whitespaces)
        
        let currentWeatherURL = URL(string: "http://api.openweathermap.org/data/2.5/weather?q=\(trimmedString)&APPID=ef47c7d28b063d37ef92c6da198c6577")! // Setting up my url, force unwrap it to prove that it's there.
        Alamofire.request(currentWeatherURL).responseJSON { response in
            let result = response.result
            if let dict = result.value as? Dictionary<String,AnyObject> {
                if let name = dict["name"] as? String {
                    self._cityName = name.capitalized
                    print("\(String(describing: self._cityName))")
                    
                }
                
                if let weather = dict["weather"] as? [Dictionary <String,AnyObject> ] {
                    if let main = weather[0]["main"] as? String {
                        self._weatherType = main.capitalized
                    }
                }
                
                if let main = dict["main"] as? Dictionary<String,AnyObject> {
                    if let currentTemperature = main["temp"] as? Double {
                        
                       // let celsiusTemp = currentTemperature - 273.15
                       // self._currentTemp = celsiusTemp
                        self._currentTemp = kelvinToFarenheit(kelvin: currentTemperature) // calculating farenheit Temperature
                        
                    }
                }
                
                if let main1 = dict["main"] as? Dictionary<String,AnyObject> {
                    if let humadity = main1["humidity"] as? Double {
                        
                        self._humadity = humadity //
                    }
                }
            }
            completed()
        }
    }
    
}
