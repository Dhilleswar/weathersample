//
//  WeekForecast.swift
///  WeatherSample
//
//  Created by GEIDC on 1/5/19.
//  Copyright © 2019 Techmahindra. All rights reserved.
//

import UIKit
import Alamofire
/*
    // Writing CustomClass With CurrentWeather and weekforecast Details
*/
class WeekForecast {
    
    var _date: String!
    var _weatherType: String!
    var _highTemp: String!
    var _lowTemp: String!
    
    var date: String {
        if _date == nil {
            _date = ""
        }
        return _date
    }
    
    var weatherType: String {
        if _weatherType == nil {
            _weatherType = ""
        }
        return _weatherType
    }
    
    var highTemp: String {
        if _highTemp == nil {
            _highTemp = ""
        }
        return _highTemp
    }
    
    var lowTemp: String {
        if _lowTemp == nil {
            _lowTemp = ""
        }
        return _lowTemp
    }
    
    init(weatherDict: Dictionary<String, AnyObject>){
        
        if let temp = weatherDict["main"] as? Dictionary<String, AnyObject> {
            
            if let min = temp["temp_min"] as? Double {
                self._lowTemp = "\(kelvinToFarenheit(kelvin: min))"
            }
            
            if let max = temp["temp_max"] as? Double {
                self._highTemp = "\(kelvinToFarenheit(kelvin: max))"
            }
        }
        
        if let weather = weatherDict["weather"] as? [Dictionary<String, AnyObject>] {
            
            if let main = weather[0]["main"] as? String {
                
                self._weatherType = main.capitalized
                //print("----------\(self._weatherType)")

            }
        }
        
        if let date = weatherDict["dt"] as? Double {
           
            let unixConvertDate = Date(timeIntervalSince1970: date)
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .full
            dateFormatter.dateFormat = "EEEE"
            dateFormatter.timeStyle = .none
            self._date = unixConvertDate.dayOfTheWeek()
            
            
        }
        
    }
}

extension Date {
    func dayOfTheWeek() -> String {
        print(String())
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE,'at' hh:mm a"
        return dateFormatter.string(from: self) // self becuase we are getting the date from self
    }
} // extension needs to be build outside of our class
