//
//  Constants.swift
//  WeatherSample
//
//  Created by GEIDC on 1/5/19.
//  Copyright © 2019 Techmahindra. All rights reserved.
//

import Foundation

let BASE_URL:String = "http://api.openweathermap.org/data/2.5/weather?"
let LATITUDE:String = "lat=39.7392"
let LONGITUDE:String = "&lon=-104.9903"
let APP_ID:String = "&appid="
let API_KEY:String = "ef47c7d28b063d37ef92c6da198c6577"

/*
 // converting Temperature Kelvin to Farenheit
 */
func kelvinToFarenheit(kelvin:Double) -> Double {
    let kelvinToFarenheitPreDivision = (kelvin * (9/5) - 459.67)
    let kelvinToFarenheit = Double(round(10 * kelvinToFarenheitPreDivision/10))
    return kelvinToFarenheit
}
typealias DownloadComplete = () -> () // This is going to tell our function when we are complete.


