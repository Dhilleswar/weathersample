//
//  DownloadWeekForecast.swift
//  WeatherSample
//
//  Created by GEIDC on 1/5/19.
//  Copyright © 2019 Techmahindra. All rights reserved.
//
import Foundation
import Alamofire

var weekForecast: WeekForecast!
var weekForecasts = [WeekForecast]()


/*
 // Downloading WeekForecast based on selected City
 */
func downloadWeekForecast(completed: @escaping DownloadComplete){
    
    let username:String? = ",ind"
    let searchString = UserDefaults.standard.string(forKey: "Key")!
    
    let trimmedString = searchString.trimmingCharacters(in: .whitespaces)
    
    let message = trimmedString + username!

    let currentWeekForecastURL = URL(string: "https://api.openweathermap.org/data/2.5/forecast?q=\(message)&cnt=20&appid=ef47c7d28b063d37ef92c6da198c6577")!
    //weekForecast = WeekForecast()
    
    Alamofire.request(currentWeekForecastURL).responseJSON { response in
        let result = response.result
        if let dict = result.value as? Dictionary<String, AnyObject> {
            if let list = dict["list"] as? [Dictionary<String, AnyObject>] {
                
                for obj in list {
                    let forecast = WeekForecast(weatherDict: obj)
                    weekForecasts.append(forecast)
                }
                weekForecasts.remove(at: 0)
                
            }
        }
    
    completed()
    }
}
