//
//  WeatherdashBoardViewController.swift
//  WeatherSample
//
//  Created by GEIDC on 1/5/19.
//  Copyright © 2019 Techmahindra. All rights reserved.
//

import UIKit

class WeatherdashBoardViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var currentTempLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var currentWeatherImage: UIImageView!
    @IBOutlet weak var currentWeatherLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var humadityLabel: UILabel!
    @IBOutlet var backButton: UIButton!
    
   var selectedCity: String!
    var currentWeather: CurrentWeather!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.async {
            self.GettingWeatherDetails()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        dateLabel.text = ""
        currentTempLabel.text = ""
        currentWeatherLabel.text = ""
        locationLabel.text = ""
        humadityLabel.text = ""
        tableView.delegate = self
        tableView.dataSource = self
        currentWeather = CurrentWeather() // in order to access your class

        // Do any additional setup after loading the view.
    }
    
    /*
     // Getting latest Weather details Based on Entered City
     */
    
    func GettingWeatherDetails() {
    currentWeather.downloadWeatherDetails {
            
            downloadWeekForecast {
                
                // self because you are doing this inside of this closure
                 print("\(weekForecasts.count)")
                if weekForecasts.count>0 {
                     self.updateMainUI()
                     self.tableView.reloadData()
                } else{
                    let alert = UIAlertController(title: "WeatherSample", message: "Please Select Metropolitan City", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { action in
                          self.backFunctionality()
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
               
                
            }
            
            
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weekForecasts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath) as? TableCell {
            let weekForecast = weekForecasts[indexPath.row]
            cell.configureCell(weekForecast: weekForecast)
            
            return cell
        } else {
            return TableCell()
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
       self.backFunctionality()
    }
    
    /*
     // Go to Previous screen
     */
    func backFunctionality() {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: ViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                weekForecasts.removeAll()
                break
            }
        }
    }
    /*
     // Updating the Dashboard userinterface with latest Weather details
     */
    func updateMainUI(){
        dateLabel.text = currentWeather.date
        currentTempLabel.text = "\(currentWeather.currentTemp) °F"
        currentWeatherLabel.text = currentWeather.weatherType
        locationLabel.text = currentWeather.cityName
        humadityLabel.text = "Humadity: \(currentWeather.humadity)"
        currentWeatherImage.image = UIImage(named: currentWeather.weatherType)
        backButton.setImage(UIImage(named: "left-arrow.png"), for: .normal)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
