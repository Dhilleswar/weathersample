//
//  ViewController.swift
//  WeatherSample
//
//  Created by GEIDC on 1/4/19.
//  Copyright © 2019 Techmahindra. All rights reserved.
//

import UIKit

class ViewController: UIViewController ,UITextFieldDelegate{

    @IBOutlet weak var searchTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        searchTextField.delegate=self;
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        searchTextField.text = ""
    }

    /*
     // Searching City based On User input
     */
    @IBAction func SearchButtonAction(_ sender: Any) {
        
        if let text = searchTextField.text, !text.isEmpty
        {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WeatherdashBoardViewController") as? WeatherdashBoardViewController
            vc?.selectedCity = searchTextField.text
            UserDefaults.standard.set(searchTextField.text, forKey: "Key")
            self.navigationController?.pushViewController(vc!, animated: true)
        }else{
            
            let alert = UIAlertController(title: "WeatherSample", message: "Please Enter Your City", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }

    }
    
    func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
}

