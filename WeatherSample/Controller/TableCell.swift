//
//  TableCell.swift
//  WeatherSample
//
//  Created by GEIDC on 1/5/19.
//  Copyright © 2019 Techmahindra. All rights reserved.
//

import UIKit

class TableCell: UITableViewCell {

    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var weatherType: UILabel!
    @IBOutlet weak var highTemp: UILabel!
    @IBOutlet weak var lowTemp: UILabel!
    
    
    
    /*
     // Upading Tableview cell with Weather Details
     */
    func configureCell(weekForecast: WeekForecast){
        lowTemp.text = "Low:\(weekForecast.lowTemp) °F"
        highTemp.text = "High:\(weekForecast.highTemp) °F"
        weatherType.text = "\(weekForecast.weatherType)"
        dayLabel.text = weekForecast.date
        weatherIcon.image = UIImage(named: weekForecast.weatherType)
    }

    

}
